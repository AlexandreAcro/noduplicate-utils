# Скрипты для управлением хранилищем

Здесь 2 скрипта.

Тот, который для удаления дубликатов, предназначен для работы в паре с программой CCleaner. 
Дело в том, что на динамических томах эта программа почему-то не может удалять дубли, хотя находит. 
Поэтому я экспортирую все находки в список и рядом с этим списком запускаю скрипт удаления дулей.
Этот скрипт генерирует bat-файл, который я могу проверить для уверенности, после чего запустить.
На Windows 7 и старше всё работает.

Второй скрипт довольно прост, так что с ним не составит проблем разобраться. 
По сути, он проверяет дерево папок в поиске файлов, расширения которых не совпадают с заданными. 
Этот скрипт помещается рядом с папкой "Изображения" фото-архива и запускается. 
В консоли он выводит список путей к файлам, которым не место в фото-архиве. 
Небольшим редактированием этого скрипта можно добиться чтобы он проверял и видео-архив.